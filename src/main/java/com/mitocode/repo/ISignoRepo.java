package com.mitocode.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Signo;

public interface ISignoRepo extends IGenericRepo<Signo, Integer>  {

	@Query(value="select s.* from signo s where s.id_paciente = :id_paciente", nativeQuery = true)
	List<Signo> listarSignoPorPaciente(@Param("id_paciente") Integer idPaciente);
	
}
