package com.mitocode.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Signo;

public interface ISignoService extends ICRUD<Signo, Integer> {

	List<Signo> listarSignoPorPaciente(Integer idPaciente);
	
	Page<Signo> listarPageable(Pageable pageable);
}
